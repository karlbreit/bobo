object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'FormMain'
  ClientHeight = 459
  ClientWidth = 680
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = 11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 11
  object Label1: TLabel
    Left = 408
    Top = 392
    Width = 40
    Height = 11
    Caption = 'Servo No'
  end
  object Label2: TLabel
    Left = 152
    Top = 397
    Width = 26
    Height = 11
    Caption = 'Label2'
  end
  object Button1: TButton
    Left = 528
    Top = 40
    Width = 75
    Height = 25
    Caption = 'INTIILISE'
    TabOrder = 0
    OnClick = Button1Click
  end
  object ComboBox1: TComboBox
    Left = 498
    Top = 389
    Width = 145
    Height = 19
    TabOrder = 1
    Text = 'ComboBox1'
  end
  object Button2: TButton
    Left = 568
    Top = 339
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 424
    Top = 341
    Width = 121
    Height = 19
    TabOrder = 3
    Text = 'Edit1'
  end
  object Button3: TButton
    Left = 32
    Top = 392
    Width = 75
    Height = 25
    Caption = 'ADC 2'
    TabOrder = 4
    OnClick = Button3Click
  end
  object EditServo1: TEdit
    Left = 32
    Top = 184
    Width = 65
    Height = 19
    TabOrder = 5
    Text = 'EditServo1'
  end
  object EditServo2: TEdit
    Left = 103
    Top = 190
    Width = 65
    Height = 19
    TabOrder = 6
    Text = 'Edit2'
  end
  object EditServo5: TEdit
    Left = 316
    Top = 184
    Width = 65
    Height = 19
    TabOrder = 7
    Text = 'Edit2'
  end
  object EditServo4: TEdit
    Left = 245
    Top = 184
    Width = 65
    Height = 19
    TabOrder = 8
    Text = 'Edit2'
  end
  object EditServo3: TEdit
    Left = 174
    Top = 184
    Width = 65
    Height = 19
    TabOrder = 9
    Text = 'Edit2'
  end
  object EditServo6: TEdit
    Left = 387
    Top = 184
    Width = 65
    Height = 19
    TabOrder = 10
    Text = 'Edit2'
  end
  object Button4: TButton
    Left = 490
    Top = 182
    Width = 75
    Height = 25
    Caption = 'POS 1'
    TabOrder = 11
    OnClick = Button4Click
  end
  object ToggleSwitch1: TToggleSwitch
    Left = 200
    Top = 312
    Width = 70
    Height = 20
    TabOrder = 12
  end
  object Button5: TButton
    Left = 528
    Top = 112
    Width = 75
    Height = 25
    Caption = 'HOME'
    TabOrder = 13
    OnClick = Button5Click
  end
  object Edit2Servo1: TEdit
    Left = 32
    Top = 215
    Width = 65
    Height = 19
    TabOrder = 14
    Text = 'EditServo1'
  end
  object Edit2Servo2: TEdit
    Left = 103
    Top = 215
    Width = 65
    Height = 19
    TabOrder = 15
    Text = 'Edit2'
  end
  object Edit2Servo5: TEdit
    Left = 316
    Top = 215
    Width = 65
    Height = 19
    TabOrder = 16
    Text = 'Edit2'
  end
  object Edit2Servo4: TEdit
    Left = 245
    Top = 215
    Width = 65
    Height = 19
    TabOrder = 17
    Text = 'Edit2'
  end
  object Edit2Servo3: TEdit
    Left = 174
    Top = 215
    Width = 65
    Height = 19
    TabOrder = 18
    Text = 'Edit2'
  end
  object Edit2Servo6: TEdit
    Left = 387
    Top = 215
    Width = 65
    Height = 19
    TabOrder = 19
    Text = 'Edit2'
  end
  object Button6: TButton
    Left = 490
    Top = 213
    Width = 75
    Height = 25
    Caption = 'POS 2'
    TabOrder = 20
    OnClick = Button6Click
  end
  object Edit3Servo1: TEdit
    Left = 32
    Top = 246
    Width = 65
    Height = 19
    TabOrder = 21
    Text = 'EditServo1'
  end
  object Edit3Servo2: TEdit
    Left = 103
    Top = 246
    Width = 65
    Height = 19
    TabOrder = 22
    Text = 'Edit2'
  end
  object Edit3Servo5: TEdit
    Left = 316
    Top = 246
    Width = 65
    Height = 19
    TabOrder = 23
    Text = 'Edit2'
  end
  object Edit3Servo4: TEdit
    Left = 245
    Top = 246
    Width = 65
    Height = 19
    TabOrder = 24
    Text = 'Edit2'
  end
  object Edit3Servo3: TEdit
    Left = 174
    Top = 246
    Width = 65
    Height = 19
    TabOrder = 25
    Text = 'Edit2'
  end
  object Edit3Servo6: TEdit
    Left = 387
    Top = 246
    Width = 65
    Height = 19
    TabOrder = 26
    Text = 'Edit2'
  end
  object Button7: TButton
    Left = 490
    Top = 244
    Width = 75
    Height = 25
    Caption = 'POS 3'
    TabOrder = 27
    OnClick = Button7Click
  end
  object Edit2: TEdit
    Left = 316
    Top = 280
    Width = 121
    Height = 19
    TabOrder = 28
    Text = 'Edit2'
  end
  object Button8: TButton
    Left = 490
    Top = 275
    Width = 75
    Height = 25
    Caption = 'Button8'
    TabOrder = 29
    OnClick = Button8Click
  end
  object Timer1: TTimer
    Interval = 500
    OnTimer = Timer1Timer
    Left = 48
    Top = 296
  end
end
