
unit Mains;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
    System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, CPortCtl, CPort,
  Vcl.ExtCtrls, Vcl.WinXCtrls;

type

  TFormMain=class(TForm)
    Label1:TLabel;
    Button1: TButton;
    ComboBox1: TComboBox;
    Button2: TButton;
    Edit1: TEdit;
    Label2: TLabel;
    Button3: TButton;
    Timer1: TTimer;
    EditServo1: TEdit;
    EditServo2: TEdit;
    EditServo5: TEdit;
    EditServo4: TEdit;
    EditServo3: TEdit;
    EditServo6: TEdit;
    Button4: TButton;
    ToggleSwitch1: TToggleSwitch;
    Button5: TButton;
    Edit2Servo1: TEdit;
    Edit2Servo2: TEdit;
    Edit2Servo5: TEdit;
    Edit2Servo4: TEdit;
    Edit2Servo3: TEdit;
    Edit2Servo6: TEdit;
    Button6: TButton;
    Edit3Servo1: TEdit;
    Edit3Servo2: TEdit;
    Edit3Servo5: TEdit;
    Edit3Servo4: TEdit;
    Edit3Servo3: TEdit;
    Edit3Servo6: TEdit;
    Button7: TButton;
    Edit2: TEdit;
    Button8: TButton;

    //COM CREATION START
    procedure MakeComPannel();
    //COM CREATION END
    procedure SendAllServoData();
    procedure FormCreate(Sender:TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure MoveServo(ServoNo:Integer; Value:Integer);
    procedure Button3Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
  //  Function RoundingUserDefineDecaimalPart(FloatNum: Double; NoOfDecPart: integer): Double;

    private{ Private declarations }


  //COM CREATION START
    procedure ComConnect(Sender:TObject);
    procedure ComDisConnect(Sender:TObject);
    procedure ComComboClicked(Sender:TObject);
    procedure ComReceive(Sender:TObject;count:integer);
    //COM CREATION END

  public{ Public declarations }
  end;

var

ADCreadFLAG:Integer;

  FormMain:TFormMain;
  //COM CREATION START
  BTN_CONNECT:Tbutton;
  ComPort1:TComPort;
  PNL_ComPort:TPanel;
  LED_ComPort1:TComLed;
  ComComboBox1:TComComboBox;
  BTN_DISCONNECT:TButton;
  //COM CREATION END

  var
  Offset_Srevo1:Integer=+325;
  Offset_Srevo2:Integer=-125;
  Offset_Srevo3:Integer=+050;
  Offset_Srevo4:Integer=-175;
  Offset_Srevo5:Integer=+150;
  Offset_Srevo6:Integer=-250;  //375
  TargetAngle_Srevo1:Integer=3000;
  TargetAngle_Srevo2:Integer=3000;
  TargetAngle_Srevo3:Integer=3000;
  TargetAngle_Srevo4:Integer=3000;
  TargetAngle_Srevo5:Integer=3000;
  TargetAngle_Srevo6:Integer=3000;
implementation

{$R *.dfm}

procedure TFormMain.FormCreate(Sender:TObject);
begin
  MakeComPannel();


  combobox1.Items.Add('ALL SERVO');
  combobox1.Items.Add('SERVO 1');
  combobox1.Items.Add('SERVO 2');
  combobox1.Items.Add('SERVO 3');
  combobox1.Items.Add('SERVO 4');
  combobox1.Items.Add('SERVO 5');
  combobox1.Items.Add('SERVO 6');
  combobox1.Items.Add('SERVO 7');
  combobox1.Items.Add('SERVO 8');
  combobox1.ItemIndex:=0;

end;



Function RoundingUserDefineDecaimalPart(FloatNum: Double; NoOfDecPart: integer): Double;
Var
     ls_FloatNumber: String;
Begin
     ls_FloatNumber := FloatToStr(FloatNum);
     IF Pos('.', ls_FloatNumber) > 0 Then
          Result := StrToFloat
            (copy(ls_FloatNumber, 1, Pos('.', ls_FloatNumber) - 1) + '.' + copy
                 (ls_FloatNumber, Pos('.', ls_FloatNumber) + 1, NoOfDecPart))
     Else
          Result := FloatNum;
End;





//COM CREATION START
procedure TFormMain.MakeComPannel();
{
var
  PNL_ComPort:TPanel;
  LED_ComPort1:TComLed;
  ComComboBox1:TComComboBox;
  BTN_CONNECT:TButton;
  BTN_DISCONNECT:TButton;
  ComPort1:TComPort;
  }
begin

  ComPort1 := TComPort.Create(Self);
  PNL_ComPort := TPanel.Create(FormMain);
  LED_ComPort1 := TComLed.Create(Self);
  ComComboBox1 := TComComboBox.Create(Self);
  BTN_CONNECT := TButton.Create(Self);
  BTN_DISCONNECT := TButton.Create(Self);

  ComPort1.Name := 'ComPort1';
  ComPort1.BaudRate := br9600;
  ComPort1.Port := 'COM1';
  ComPort1.StopBits := sbOneStopBit;
  ComPort1.DataBits := dbEight;
  ComPort1.Events := [evRxChar, evTxEmpty, evRxFlag, evRing, evBreak, evCTS,
    evDSR, evError, evRLSD, evRx80Full];
  ComPort1.StoredProps := [spBasic];
  ComPort1.TriggersOnRxChar := True;
  comport1.DiscardNull:=false;

  PNL_ComPort.Name := 'PNL_ComPort';
  PNL_ComPort.Parent := formmain;
  PNL_ComPort.Left := 8;
  PNL_ComPort.Top := 8;
  PNL_ComPort.Width := 193;
  PNL_ComPort.Height := 105;
  PNL_ComPort.Caption := 'Com Port Connection';
  PNL_ComPort.TabOrder := 0;
  PNL_ComPort.VerticalAlignment := taAlignTop;
  LED_ComPort1.Name := 'LED_ComPort1';
  LED_ComPort1.Parent := PNL_ComPort;
  LED_ComPort1.Left := 152;
  LED_ComPort1.Top := 28;
  LED_ComPort1.Width := 25;
  LED_ComPort1.Height := 25;
  LED_ComPort1.LedSignal := lsConn;
  LED_ComPort1.Kind := lkRedLight;
  LED_ComPort1.ComPort := comport1;
  ComComboBox1.Name := 'ComComboBox1';
  ComComboBox1.Parent := PNL_ComPort;
  ComComboBox1.Left := 8;
  ComComboBox1.Top := 28;
  ComComboBox1.Width := 138;
  ComComboBox1.Height := 21;
  ComComboBox1.Text := '';
  ComComboBox1.Style := csDropDownList;
  ComComboBox1.ItemIndex := 1;
  ComComboBox1.TabOrder := 0;
  comcombobox1.ComPort := ComPort1;
  comcombobox1.ComProperty := cpPort;
  BTN_CONNECT.Name := 'BTN_CONNECT';
  BTN_CONNECT.Parent := PNL_ComPort;
  BTN_CONNECT.Left := 8;
  BTN_CONNECT.Top := 72;
  BTN_CONNECT.Width := 75;
  BTN_CONNECT.Height := 25;
  BTN_CONNECT.Caption := 'CONNECT';
  BTN_CONNECT.TabOrder := 1;
  BTN_DISCONNECT.Name := 'BTN_DISCONNECT';
  BTN_DISCONNECT.Parent := PNL_ComPort;
  BTN_DISCONNECT.Left := 89;
  BTN_DISCONNECT.Top := 72;
  BTN_DISCONNECT.Width := 88;
  BTN_DISCONNECT.Height := 25;
  BTN_DISCONNECT.Caption := 'DISCONNECT';
  BTN_DISCONNECT.TabOrder := 2;

  BTN_connect.onClick := ComConnect;
  BTN_disconnect.onClick := Comdisconnect;
  comcombobox1.OnClick := ComComboClicked;
  comport1.OnRxChar:=Comreceive;

end;

procedure TFormMain.ComConnect(Sender:TObject);
begin
  //comport1.Port := comcombobox1.Text;
  comport1.Connected := true;
end;



procedure TFormMain.ComDisConnect(Sender:TObject);
begin
   comport1.Connected:=False;
  label1.Caption := comport1.Port;
end;

procedure TFormMain.Button1Click(Sender: TObject);
begin
comport1.TransmitChar('U');
end;

procedure TFormMain.Button2Click(Sender: TObject);
var
i:integer;
begin
if combobox1.ItemIndex = 0 then
begin
 i:=1;
while i<=8 do
begin
 MoveServo(i, StrToInt(Edit1.Text));
i:=i+1;
end;

end
else
begin
  MoveServo(combobox1.ItemIndex, StrToInt(Edit1.Text));
end;

end;

procedure TFormMain.Button3Click(Sender: TObject);

var
temp:integer;
delay:Integer;
b1,b2,b3:byte;
begin
ADCreadFLAG:=1;
  b1:=128;
comport1.TransmitChar(char(b1));
b1:=1;
comport1.TransmitChar(char(b1));
b1:=64;
comport1.TransmitChar(char(b1));
b1:=2;
comport1.TransmitChar(char(b1));
b1:=0;
comport1.TransmitChar(char(b1));
b1:=0;
comport1.TransmitChar(char(b1));
end;

procedure TFormMain.Button4Click(Sender: TObject);
begin
TargetAngle_Srevo1:=STRtoINT(EditServo1.Text);
TargetAngle_Srevo2:=6000-(STRtoINT(EditServo2.Text));
TargetAngle_Srevo3:=STRtoINT(EditServo3.Text);
TargetAngle_Srevo4:=6000-STRtoINT(EditServo4.Text);
TargetAngle_Srevo5:=STRtoINT(EditServo5.Text);
TargetAngle_Srevo6:=6000-STRtoINT(EditServo6.Text);
end;

procedure TFormMain.Button5Click(Sender: TObject);
begin

TargetAngle_Srevo1:=3000;
TargetAngle_Srevo2:=3000;
TargetAngle_Srevo3:=3000;
TargetAngle_Srevo4:=3000;
TargetAngle_Srevo5:=3000;
TargetAngle_Srevo6:=3000;
end;

procedure TFormMain.Button6Click(Sender: TObject);
begin
TargetAngle_Srevo1:=STRtoINT(Edit2Servo1.Text);
TargetAngle_Srevo2:=6000-(STRtoINT(Edit2Servo2.Text));
TargetAngle_Srevo3:=STRtoINT(Edit2Servo3.Text);
TargetAngle_Srevo4:=6000-STRtoINT(Edit2Servo4.Text);
TargetAngle_Srevo5:=STRtoINT(Edit2Servo5.Text);
TargetAngle_Srevo6:=6000-STRtoINT(Edit2Servo6.Text);
end;

procedure TFormMain.Button7Click(Sender: TObject);
begin
TargetAngle_Srevo1:=STRtoINT(Edit3Servo1.Text);
TargetAngle_Srevo2:=6000-(STRtoINT(Edit3Servo2.Text));
TargetAngle_Srevo3:=STRtoINT(Edit3Servo3.Text);
TargetAngle_Srevo4:=6000-STRtoINT(Edit3Servo4.Text);
TargetAngle_Srevo5:=STRtoINT(Edit3Servo5.Text);
TargetAngle_Srevo6:=6000-STRtoINT(Edit3Servo6.Text);
end;

procedure TFormMain.Button8Click(Sender: TObject);
begin
TargetAngle_Srevo1:=STRtoINT(Edit2.Text);
TargetAngle_Srevo2:=6000-(STRtoINT(Edit2.Text));
TargetAngle_Srevo3:=STRtoINT(Edit2.Text);
TargetAngle_Srevo4:=6000-STRtoINT(Edit2.Text);
TargetAngle_Srevo5:=STRtoINT(Edit2.Text);
TargetAngle_Srevo6:=6000-STRtoINT(Edit2.Text);
end;

procedure TFormMain.ComComboClicked(Sender:TObject);
begin
    if comcombobox1.Text <> '' then
    begin
      comport1.Port:=comcombobox1.Text;
    end;
end;

procedure TFormMain.ComReceive(Sender:TObject;count:integer);
var
 recdata:String;
 b1:byte;
 voltage:Integer;
 voltage2:extended;
begin
recdata:='   ';
comport1.ReadStr(recdata,1);
voltage:= Integer(recdata[1]);
voltage:=voltage*5*2;
voltage2:=voltage/255;
     if ADCreadFlag=1 then
     begin
       ADCreadFLAG:=0;
       label2.Caption:=floattostr(RoundingUserDefineDecaimalPart(voltage2, 2));
     end;
end;
//COM CREATION END



procedure TFormMain.MoveServo(ServoNo:Integer; Value:Integer);
var
temp:integer;
delay:Integer;
b1,b2,b3:byte;
begin
b1:=128;
comport1.TransmitChar(char(b1));
b1:=1;
comport1.TransmitChar(char(b1));
b1:=1;
comport1.TransmitChar(char(b1));
b1:=ServoNo;
comport1.TransmitChar(char(b1));

delay:=Value;
temp:=trunc(delay/128);
b1:=temp;
b2:=delay mod 128;
comport1.TransmitChar(char(b1));
comport1.TransmitChar(char(b2));


end;

procedure TFormMain.SendAllServoData();
begin
   MoveServo(1,(Offset_Srevo1+TargetAngle_Srevo1));
   MoveServo(2,(Offset_Srevo2+TargetAngle_Srevo2));
   MoveServo(3,(Offset_Srevo3+TargetAngle_Srevo3));
   MoveServo(4,(Offset_Srevo4+TargetAngle_Srevo4));
   MoveServo(5,(Offset_Srevo5+TargetAngle_Srevo5));
   MoveServo(6,(Offset_Srevo6+TargetAngle_Srevo6));
end;


procedure TFormMain.Timer1Timer(Sender: TObject);
begin
if ToggleSwitch1.State = TssOn
then
begin
 SendAllServoData();
end;
end;

end.


